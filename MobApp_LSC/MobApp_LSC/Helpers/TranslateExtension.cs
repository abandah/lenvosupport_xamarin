﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobApp_LSC.Helpers
{
	[ContentProperty("Text")]
	public class TranslateExtension : IMarkupExtension
	{
		public string Text { get; set; }

		public object ProvideValue(IServiceProvider serviceProvider)
		{
			if (Text == null)
				return null;

			var assembly = typeof(TranslateExtension).GetTypeInfo().Assembly;
			var assemblyName = assembly.GetName();
			ResourceManager resourceManager = new ResourceManager($"{assemblyName.Name}.Resources", assembly);

			String s =  resourceManager.GetString(Text, CultureInfo.CurrentCulture);
			return s;
		}

		public static string Translate(string Text)
		{
			if (Text == null)
				return null;

			var assembly = typeof(TranslateExtension).GetTypeInfo().Assembly;
			var assemblyName = assembly.GetName();
			ResourceManager resourceManager = new ResourceManager($"{assemblyName.Name}.Resources", assembly);

			String s =  resourceManager.GetString(Text, CultureInfo.CurrentCulture);
			return s;
		}
	}
	//	[ContentProperty("Text")]
	//    public class TranslateExtension : IMarkupExtension
	//    {
	//        const string ResourceId = " MobApp_LSC.AppResources";

	//        static readonly Lazy<ResourceManager> resmgr = new Lazy<ResourceManager>(() => new ResourceManager(ResourceId, typeof(TranslateExtension).GetTypeInfo().Assembly));

	//        public string Text { get; set; }

	//        public object ProvideValue(IServiceProvider serviceProvider)
	//        {
	//            if (Text == null)
	//                return "";

	//            var ci = CrossMultilingual.Current.CurrentCultureInfo;

	//            var translation = resmgr.Value.GetString(Text, ci);

	//            if (translation == null)
	//            {

	//#if DEBUG
	//                throw new ArgumentException(
	//                    String.Format("Key '{0}' was not found in resources '{1}' for culture '{2}'.", Text, ResourceId, ci.Name),
	//                    "Text");
	//#else
	//				translation = Text; // returns the key, which GETS DISPLAYED TO THE USER
	//#endif
	//            }
	//            return translation;
	//        }
	//    }
}