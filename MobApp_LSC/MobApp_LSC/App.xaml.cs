﻿using Acr.UserDialogs;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Push;
using MobApp_LSC.Files.SignInViews;
using MobApp_LSC.Files.Views;
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MobApp_LSC
{
	public partial class App : Xamarin.Forms.Application
	{
		//yyy
		public static string UserName { get; set; }
		public static ImageSource ImageUser { get; set; }
		public static string Occupation { get; private set; }
		public static Page Detail { get; internal set; }
		private string data;

		public App()
		{
			InitializeComponent();
			MainPage = new SplashPage(null);
		}

		public App(string data)
		{
			InitializeComponent();

			this.data = data;
			Xamarin.Forms.PlatformConfiguration.AndroidSpecific.Application.SetWindowSoftInputModeAdjust(this, Xamarin.Forms.PlatformConfiguration.AndroidSpecific.WindowSoftInputModeAdjust.Resize);

			MainPage = new SplashPage(data);
		}

		protected override void OnStart()
		{
			
			//AppCenter.Start("a5d33937-598c-488e-8bdf-ac9ea4c93537", typeof(Push));
			//Push.SetEnabledAsync(true);
			//AppCenter.Start("5ad56dfa-5208-4659-9db8-59541d269595;", typeof(Analytics), typeof(Crashes));
			//AppCenter.Start("5ad56dfa-5208-4659-9db8-59541d269595", typeof(Push));
			//AppCenter.Start("a505c78c-b749-4c50-a217-04caab1d1e55;", typeof(Analytics), typeof(Crashes));

		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}

		//public async static void GetGoogleCode(string queryParameter)
		//{
		//    try
		//    {
		//        if (queryParameter != "")
		//        {
		//            GoogleViewModel _vm = new GoogleViewModel();
		//            var accessToken = await _vm.GetAccessTokenAsync(queryParameter);

		//            await _vm.SetGoogleUserProfileAsync(accessToken);
		//            App.UserName = _vm.GoogleProfile.DisplayName;
		//            string photo = _vm.GoogleProfile.Image.Url;
		//            App.Occupation = _vm.GoogleProfile.Occupation;

		//            Debug.WriteLine(App.UserName);
		//            App.ImageUser = ImageSource.FromUri(new Uri(photo));
		//            App.Current.MainPage = new GoogleProfileCsPage();
		//        }
		//    }
		//    catch (Exception error)
		//    {
		//        Debug.WriteLine(error.Message);
		//    }
		//}
	}
}
