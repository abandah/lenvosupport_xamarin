﻿using MobApp_LSC.Files;
using MobApp_LSC.Files.constant;
using MobApp_LSC.Files.Interfaces;
using MobApp_LSC.Files.Refit;
using NavigationDrawer.MenuItems;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobApp_LSC
{
	public partial class MainPage : ContentPage, OnResponse<EntryTypes>
	{//sdfdsf
		ItemListViewModel itemListViewModel;

		public MainPage()
		{
			InitializeComponent();
			
			CallApi_GetEntryTypes();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			//CallApi_GetMenuItems();

		}
		private void CallApi_GetEntryTypes()
		{
			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_USERID, "4" },
				{ WSConstants.PARAM_LENVO_TOKEN, "uy46u5y5uyu5y5uy" },};

			CallApi.HandelErrors(GetEntryTypes, data, WSConstants.GET_ENTRY_TYPES, this);
		}
		private async Task<WSResponse<EntryTypes>> GetEntryTypes(Dictionary<string, string> data)
		{

			LenvoJobsAPIs<EntryTypes> EntryTypes = CallApi.Caller<LenvoJobsAPIs<EntryTypes>>(WSConstants.API_URLs);
			//   registerAPI.Registration(data).Wait();
			WSResponse<EntryTypes> wSResponse = await EntryTypes.GetEntryTypes(data);
			//  Debug.WriteLine(wSResponse.getMessage());
			return wSResponse;
		}
		void OnResponse<EntryTypes>.OnResult(int CallId, WSResponse<EntryTypes> response)
		{

			switch (CallId)
			{
				case WSConstants.GET_ENTRY_TYPES:
					OnResult_CallApi_GET_ENTRY_TYPES(CallId, response);
					break;

			}

		}
		private void OnResult_CallApi_GET_ENTRY_TYPES(int callId, WSResponse<EntryTypes> response)
		{
			if (response.Status == 1)
			{
				itemListViewModel = new ItemListViewModel(response.dataFromServer.DataRows);
				BindingContext = itemListViewModel;
				DisplayAlert(Helpers.TranslateExtension.Translate("Success"), response.Message, Helpers.TranslateExtension.Translate("OK"));
			}
			else if (response.Status == 0)
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), response.Message, Helpers.TranslateExtension.Translate("OK"));
			}
			else
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Error"), response.Message, Helpers.TranslateExtension.Translate("OK"));

			}


		}
	}
}
