﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MobApp_LSC.Files.Interfaces
{
	public class ItemListViewModel : BaseViewModel
	{
		public ObservableCollection<EntryTypes> Posts { get; set; }

		public ItemListViewModel(List<EntryTypes> entryTypes)
		{
			this.Posts = new ObservableCollection<EntryTypes>();
		foreach(EntryTypes et in entryTypes){
				this.Posts.Add(et);
			}
		}
	}
}
