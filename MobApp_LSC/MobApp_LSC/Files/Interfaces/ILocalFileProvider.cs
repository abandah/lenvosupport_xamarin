﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobApp_LSC.Files.Interfaces
{
	public interface ILocalFileProvider
	{
		Task<string> SaveFileToDisk(System.IO.Stream stream, string fileName);
	}
}
