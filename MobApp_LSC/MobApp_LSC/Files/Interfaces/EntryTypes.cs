﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.Interfaces
{
	public class EntryTypes
    {
		public string Simple_Text { get; set; }

		public int TypeID { get; set; }
		public string TypeName { get; set; }
		public bool ReflectsOnWorkingHours { get; set; }
		public bool IsDefault { get; set; }
		public bool IsOut { get; set; }
		public object FK_VactionId { get; set; }
		public int SYS_CompanyID { get; set; }
		public int SYS_Add_Date_Time { get; set; }
		public int SYS_Add_User { get; set; }
		public int? SYS_Modify_Date { get; set; }
		public int? SYS_Modify_User { get; set; }
		public int? OutType { get; set; }
		public int? FK_LeaveVacationTypeId { get; set; }
		public bool IsAutoAssignFunction { get; set; }
	}
	public class DataFromServer
	{
		public List<EntryTypes> DataRows { get; set; }
	}

	public class RootObject
	{
		public DataFromServer DataFromServer { get; set; }
		public int Status { get; set; }
		public string Message { get; set; }
	}
}
