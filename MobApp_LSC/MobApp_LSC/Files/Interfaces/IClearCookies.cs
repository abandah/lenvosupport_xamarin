﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.Interfaces
{
	public interface IClearCookies
	{
		void Clear();
	}
}
