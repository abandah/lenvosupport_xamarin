﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.Constant
{
    public static class Users
    {

        public class Email
        {
            public string value { get; set; }
            public string type { get; set; }
        }

        public class Name
        {
            public string familyName { get; set; }
            public string givenName { get; set; }
        }

        public class Image
        {
            public string url { get; set; }
            public bool isDefault { get; set; }
        }

        public class RootObject
        {
            public string kind { get; set; }
            public string etag { get; set; }
            public List<Email> emails { get; set; }
            public string id { get; set; }
            public string displayName { get; set; }
            public Name name { get; set; }
            public Image image { get; set; }
            public string language { get; set; }
        }
    }
}