﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.constant
{
    class WSConstants
    {

        public const int UPDATE_GCM = 0;

        public const int REGISTER_DEVICE = 1;

        public const int LOGIN_DEVICE = 2;

        public const int GET_LENVO_TOKEN = 3;

        public const int REGISTER_NEW_USER = 4;

        public const int REGISTER_LINKEDIN = 5;

        public const int REGISTER_FACEBOOK = 6;

        public const int CHECK_DUPLICATE_EMAIL = 7;

        public const int FORGOT_PASSWORD = 8;

        public const int REGISTRATION_BY_GMAIL = 9;

		public const int CALL_MENU = 10;

		public const int GET_ENTRY_TYPES = 11;

		public const int GET_USER_ALLOWED_DEVICES = 12;


		public const int NoInterNet = 5000;

		public static string Link= "http://support.lenvosoft.com/";

		public const string API_URL ="WebServices/mobileapp.asmx";
		public const string API_URLs = "http://support.lenvosoft.com/";

		public  const string PARAM_USERNAME = "UserName";
	   public  const string PARAM_PASSWORD = "PassWord";
        public const string PARAM_LENVO_TOKEN = "LenvoToken";
        public const string PARAM_LENVO_LOGIN_TOKEN = "LenvoLoginToken";
        public const string PARAM_DEVIC_SERIAL = "DeviceSerial";

        public const string PARAM_FIRSTNAME = "Reg_FirstName";
        public const string PARAM_LASTNAME = "Reg_LastName";
        public const string PARAM_EMAIL = "Reg_Email";
        public const string PARAM_LINEDIN_ID = "LinkedInId";
        public const string PARAM_LINEDIN_URL = "LinkedIn_ProfileURL";
        public const string PARAM_FACEBOOK_URL = "Facebook_ProfileURL";
        public const string PARAM_GMAIL_ID = "GmailId";
        public const string PARAM_GMAIL_URL = "Gmail_ProfileUrl";
		
        public const string PARAM_DEVICE_TYPE = "DeviceType";
        public const string PARAM_GCM_TOKEN = "GcmToken";
        public const string PARAM_FACEBOOK_ID = "FacebookId";
        public const string PARAM_USERID = "UserId";
        public const string PARAM_Latitude = "Latitude";
        public const string PARAM_Longitude = "Longitude";
        public const string PARAM_UserLatitude = "UserLatitude";
        public const string PARAM_UserLongitude = "UserLongitude";
        public const string PARAM_EntryType = "EntryType";

        public const int LOCATION_ACCURACY = 25;
		
		public const string PARAM_Pic = "Employee_Image_Path";
        public const string PARAM_Gmail_Pic = "Employee_Image_Path";

        public static string OS_TYPE { get; internal set; }
    }
}
