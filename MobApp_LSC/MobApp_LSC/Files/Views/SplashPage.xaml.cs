﻿using MobApp_LSC.Files.constant;
using MobApp_LSC.Files.SignInViews;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobApp_LSC.Files.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashPage : ContentPage
	{
		private string data;

		public SplashPage(string data)
		{
			InitializeComponent();
			Global.Link = WSConstants.API_URLs;
			this.data = data;
			if (Device.OS == TargetPlatform.iOS)
			{
				constant.WSConstants.OS_TYPE = "2";
			}
			else
			{
				constant.WSConstants.OS_TYPE = "1";
			}
			Global.getDeviceSerial();
			StartTimer(Global.IsAllGloblized());
		}

		public void StartTimer(bool IsAllGloblized)
		{
			Device.StartTimer(TimeSpan.FromSeconds(5), () =>
			{
				if (IsAllGloblized == true)
				{
					StartLaunchPage();
				}
				else
				{
					StartLogIn();
				}
				return false; // True = Repeat again, False = Stop the timer
			});
		}
		public void StartLaunchPage()
		{
			ResetNavigationStack();
			App.Current.MainPage = new NavigationPage(new Views.MainPage(data))
			{
				BarBackgroundColor = Color.FromHex("#666666"),
				BarTextColor = Color.FromHex("#FFFFFF"),
				Icon = ""
			};
		}
		public void StartLogIn()
		{
			ResetNavigationStack();
			App.Current.MainPage = new NavigationPage(new SignInMainPage())
			{
				BarBackgroundColor = Color.FromHex("#666666"),
				BarTextColor = Color.FromHex("#FFFFFF"),
				Icon = ""
			};
		}
		public void ResetNavigationStack()
		{
			var existingPages = Navigation.NavigationStack.ToList();
			foreach (var page in existingPages)
			{
				Navigation.RemovePage(page);
			}
		}
	}
}