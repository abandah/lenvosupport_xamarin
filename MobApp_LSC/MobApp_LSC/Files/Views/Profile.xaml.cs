﻿using Acr.UserDialogs;
using MobApp_LSC.Files.constant;
using MobApp_LSC.Files.Interfaces;
using MobApp_LSC.Files.Refit;
using MobApp_LSC.Files.HybridWebViews;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using NavigationDrawer.MenuItems;
using Plugin.Permissions.Abstractions;
using Plugin.Permissions;
using SuaveControls.Views;
using MobApp_LSC.Files.Views;
using Newtonsoft.Json.Linq;

namespace MobApp_LSC.Files.View
{


	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Profile : ContentPage, HwvListener, OnResponse<MasterPageItem>, NoInernetConnectionListener, Listener
	{
		JObject json;
		private FloatingActionButton normalFab;
		bool done = false;
		hwvProfile hybridWebViewProfile;
		bool _isEnable = false;
		private string Redirect_url;
		private string v;
		private string currentURL;
		private bool isAttendance = false;
		private bool isWebServicRunning = false;

		public Profile()
		{
			InitializeComponent();
		}

		public Profile(string Redirect_url)
		{
			InitializeComponent();

			ToolbarItems[0].Text = "";

			isAttendance = true;

			this.Redirect_url = Redirect_url;

			CallApi_LoginDevice();

		}

		private void FloatingActionButton_Clicked(object sender, EventArgs e)
		{
			if (Util.Validation.IsConnected())
			{
				if (Device.OS == TargetPlatform.iOS)
				{
				}
				else
				{
				}
			}
			else
			{
				shadaAsync();
			}

		}

		public void OnLeftButtonClicked(object sender, EventArgs e)
		{
			if (Util.Validation.IsConnected())
			{
				hybridWebViewProfile.GoBack();
			}
			else
			{
				shdaAsync();
			}


		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			//CallApi_LoginDevice();

		}
		public void RunUri(string uri)
		{
			var tapGestureRecognizer = new TapGestureRecognizer();

			Grid g = new Grid();
			g.VerticalOptions = LayoutOptions.FillAndExpand;
			g.HorizontalOptions = LayoutOptions.FillAndExpand;

			hybridWebViewProfile = new hwvProfile
			{
				Uri = uri,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			//StartCallCheckIfPunchesExcist();

			FloatingActionButton l = new FloatingActionButton();
			l.Image = "currentlocation.png";
			l.HeightRequest = 90;
			l.WidthRequest = 79;
			l.CornerRadius = 50;
			l.IsVisible = false;
			l.VerticalOptions = LayoutOptions.EndAndExpand;
			l.HorizontalOptions = LayoutOptions.EndAndExpand;
			l.Clicked += FloatingActionButton_Clicked;

			Dictionary<string, string> datafromserver = new Dictionary<string, string> {
					{WSConstants.PARAM_USERID, Global.UserId },
				{ WSConstants.PARAM_LENVO_TOKEN, Global.getLenvoLoginToken() },
			};

			CallAPI<JObject> c = new CallAPI<JObject>();
			c.DataResponse += (sender, e) =>
			{

				if (e.Status == 1)
				{
					bool value = (bool)e.dataFromServer.DataRows[0].GetValue("IsAllowedDevice");
					if (value == false)
					{
						//l.IsVisible = false;
					}
					else
					{
						l.IsVisible = true;
					}
				}
				else
				{

				}
			};
			c.CallApi_GetResponce(CallCheckIfPunchesExcist, datafromserver);

			if (Device.OS == TargetPlatform.iOS)
			{
				l.HeightRequest = 60;
				l.WidthRequest = 50;
				l.Margin = new Thickness(0, 0, 10, 10);
				l.ButtonColor = Color.FromHex("#03A9F4");
			}

			hybridWebViewProfile.RegisterAction(data => DisplayAlert("Alert", "Hello " + data, "OK"));
			hybridWebViewProfile.setlistener(this);
			g.Children.Add(hybridWebViewProfile);
			g.Children.Add(l);
			Content = g;
		}

		public class DataRow
		{
			public string IsAllowedDevice { get; set; }
		}
		public class DataFromServer
		{
			public List<DataRow> DataRows { get; set; }
		}

		public class RootObject
		{
			public DataFromServer DataFromServer { get; set; }
			public int Status { get; set; }
			public string Message { get; set; }
		}

		public async Task<WSResponse<JObject>> CallCheckIfPunchesExcist(Dictionary<string, string> data)
		{
			LenvoJobsAPIs<JObject> interfac = CallApi.Caller<LenvoJobsAPIs<JObject>>(Global.Link + WSConstants.API_URL);
			WSResponse<JObject> wSResponse = await interfac.GetUserAllowedDevices(data);
			return wSResponse;
		}


		public void onLoaded()
		{
			//   Files.

			// DisplayAlert("Success", "Click Handled", "Great!");
		}

		public void OnProgressChanged(int newProgress)
		{

		}

		protected override bool OnBackButtonPressed()
		{
			hybridWebViewProfile.GoBack();

			return true;
		}

		async void OnAlertYesNoClicked()
		{
			if (!done)
			{
				done = true;
				bool answer = await DisplayAlert("Question?", "Would you like to play a game", "Yes", "No");
				if (answer)
				{
					//    hwvProfile.Uri = "https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/navigation/pop-ups";
					RunUri("https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/navigation/pop-ups");
					//hwvProfile.re                
				}
				Debug.WriteLine("Answer: " + answer);
			}
		}

		public async void ShouldOverrideUrlLoading(string url)
		{

			if (Util.Validation.IsConnected())
			{
				if (url.Contains("login.aspx"))
				{
					HideWebViewGetNewToken(url);
				}
				//if (url.Contains("PDFReportViewer.aspx"))
				//{
				//	try
				//	{
				//		var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
				//		if (status != PermissionStatus.Granted)
				//		{
				//			if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
				//			{
				//				await DisplayAlert("Need Camera", "LenvoHRE needs camera access", "OK");
				//			}

				//			var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
				//			//Best practice to always check that the key exists
				//			if (results.ContainsKey(Permission.Storage))
				//				status = results[Permission.Storage];
				//		}

				//		if (status == PermissionStatus.Granted)
				//		{

				//		}
				//		else if (status != PermissionStatus.Unknown)
				//		{
				//			await DisplayAlert("Camera Denied", "Can not continue, try again.", "OK");
				//		}
				//	}
				//	catch (Exception ex)
				//	{
				//		ex.ToString();
				//	}
				//	//Device.OpenUri(new Uri(url));
				//}
			}
			else
			{
				HideWebViewNoInternet(url);
			}

		}
		public void NeedRetry(string url)
		{
			HideWebViewNoInternet(url);
		}
		private void HideWebViewNoInternet(string Url)
		{
			shdaAsync();
			//Content = new NoInternetConnection(this, Url).Content;
			// Content = l;ghj
		}



		private void HideWebViewGetNewToken(string Url)
		{
			//	GC.Collect();
			this.currentURL = Url;
			//     Global.TokenGetter.Reg_Profile(this);
			//    Global.TokenGetter.AskForToken();
			//CallApi_LoginDevice();

		}

		public void OnPageStarted(string strUrl)
		{
			if (Redirect_url == null)
			{
				Redirect_url = "";
			}
			if (strUrl == Redirect_url.ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "index.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "Dashboard.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "DetailedInfo.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "QRBarcode.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "OrganizaionalStructure/Organizaional_Structure.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else
			{
				ToolbarItems[0].Text = Helpers.TranslateExtension.Translate("Back");
			}
			
			Util.ProgressDialog.HideProgressDialog();
		}

		public void OnPageFinished(string url)
		{

		}

		public void OnReceivedHttpError(string internalError)
		{

		}
		public void OnReceivedError(string ErrorType)
		{
			//if (Util.Validation.IsConnected())
			//{
			//    // hide webview 
			//    // Call API 
			//    // reload the Url 
			//}
			//else
			//{
			//    HideWebViewNoInternet("google.com");
			//}
		}
		private void CallApi_LoginDevice()
		{
			isWebServicRunning = true;

			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_USERID,Global.UserId },
					{WSConstants.PARAM_LENVO_LOGIN_TOKEN, Global.getLenvoLoginToken().Trim()},
					 { WSConstants.PARAM_DEVIC_SERIAL,Global.getDeviceSerial()},
				};

			CallApi.HandelErrors(LoginDevice, data, WSConstants.LOGIN_DEVICE, this);


		}
		private async Task<WSResponse<MasterPageItem>> LoginDevice(Dictionary<string, string> data)
		{

			LenvoJobsAPIs<MasterPageItem> LoginDevice = CallApi.Caller<LenvoJobsAPIs<MasterPageItem>>(Global.Link + WSConstants.API_URL);
			//   registerAPI.Registration(data).Wait();
			WSResponse<MasterPageItem> wSResponse = await LoginDevice.LoginDevice(data);
			//  Debug.WriteLine(wSResponse.getMessage());
			return wSResponse;
		}


		void OnResponse<MasterPageItem>.OnResult(int CallId, WSResponse<MasterPageItem> response)
		{

			switch (CallId)
			{
				case WSConstants.LOGIN_DEVICE:
					OnResult_CallApi_LoginDevice(CallId, response);
					break;

				case WSConstants.NoInterNet:
					shdaAsync();
					break;

			}

		}

		private async void shdaAsync()
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Close")
			});
			if (result)
			{
				App.Current.MainPage = new SplashPage(null);
			}
			else
			{
				//App.Current.MainPage = new Exit();
			}
		}
		private async void shadaAsync()
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Close")
			});
			if (result)
			{
				FloatingActionButton_Clicked(null, null);
			}
			else
			{
				//App.Current.MainPage = new Exit();
			}
		}

		private void OnResult_CallApi_LoginDevice(int callId, WSResponse<MasterPageItem> response)
		{
			Console.WriteLine(Redirect_url);

			if (response.Status == 1)
			{
				SavePrefAndGlobizeAsync(response.LenvoLoginToken);

			}
			else if (response.Status == 3)
			{
				dialogAPIAsync(response.Message);

			}
			else if (response.Status == 0)
			{
				dialogAPIAsync(response.Message);
			}
			else
			{
				dialogAPIAsync(response.Message);
			}
			isWebServicRunning = false;
		}
		private async void dialogAPIAsync(string message)
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = message,
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("logout")
			});
			if (result)
			{
				CallApi_LoginDevice();
			}
			else
			{
				Pref.CleanPref();
				App.Current.MainPage = new SplashPage(null);
			}
		}

		private async Task SavePrefAndGlobizeAsync(string lenvoLoginToken)
		{
			await Pref.SaveApplicationProperty(Pref.LENVO_LOGIN_TOKEN, lenvoLoginToken);

			Global.setLenvoLoginToken(Pref.LoadApplicationProperty<string>(Pref.LENVO_LOGIN_TOKEN));
			string parslinkprofile = Global.Link + "login.aspx?UserId=" + Global.UserId + "&LenvoLoginToken=" + HttpUtility.UrlEncode(lenvoLoginToken, Encoding.UTF8);

			if (this.Redirect_url != null && this.Redirect_url.Length > 0)
			{
				if (this.currentURL != null && this.currentURL.Length > 0)
				{
					parslinkprofile += "&Redirect_url=" + currentURL;
					currentURL = "";

				}
				else
				{
					parslinkprofile += "&Redirect_url=" + Redirect_url;
				}

			}
			parslinkprofile += "&IsMobileApp=true";
			string Link = parslinkprofile;


			RunUri(Link);
			//RunUri("http://yahoo.com");


		}


		public void ReloadAfterNoInernet(string url)
		{
			RunUri(url);
		}

		public void IOS_DidStartProvisionalNavigation()
		{
			if (Util.Validation.IsConnected())
			{
				Util.ProgressDialog.ShowProgressDialog();
			}
			else
			{

			}
		}


		public void IOS_DidFinishNavigation(string strUrl)
		{
			if (Redirect_url == null)
			{
				Redirect_url = "";
			}
			if (strUrl == Redirect_url.ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "index.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "Dashboard.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "DetailedInfo.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "QRBarcode.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else if (strUrl == Global.Link + "OrganizaionalStructure/Organizaional_Structure.aspx".ToLower())
			{
				ToolbarItems[0].Text = "";
			}
			else
			{
				ToolbarItems[0].Text = Helpers.TranslateExtension.Translate("Back");
			}

			Util.ProgressDialog.HideProgressDialog();
		}


		public void IOS_DecidePolicy(int statusCode)
		{
			if (statusCode == 404)
			{
				Util.ProgressDialog.HideProgressDialog();
			}
		}

		public void IOs_Retry()
		{
			if (isWebServicRunning = false)
			{
				CallApi_LoginDevice();
			}
		}

		public void RunWebView(string link)
		{
			RunUri(link);

		}

		public async Task<WSResponse<T>> GetEntryTypes<T>(Dictionary<string, string> data)
		{
			LenvoJobsAPIs<T> interfac = CallApi.Caller<LenvoJobsAPIs<T>>(Global.Link + WSConstants.API_URL);
			WSResponse<T> wSResponse = await interfac.GetEntryTypes(data);
			return wSResponse;
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();


		}
	}
}
