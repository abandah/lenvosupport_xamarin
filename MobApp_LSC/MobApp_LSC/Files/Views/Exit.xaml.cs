﻿using MobApp_LSC.Files.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobApp_LSC.Files.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Exit : ContentPage
	{
		public Exit ()
		{
			InitializeComponent ();

			System.Diagnostics.Process.GetCurrentProcess().Kill();

			//var closer = DependencyService.Get<ICloseApplication>();
			//closer.closeApplication();
			//var closer = DependencyService.Get<ICloseApplication>();
			//closer?.closeApplication();
		}
	}
}