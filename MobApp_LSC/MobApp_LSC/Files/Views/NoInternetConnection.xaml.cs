﻿using MobApp_LSC.Files.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobApp_LSC.Files.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NoInternetConnection : ContentPage
	{
        private NoInernetConnectionListener listener;
        private string Url;

        public NoInternetConnection ()
		{
			InitializeComponent ();
		}

        public NoInternetConnection( NoInernetConnectionListener listener , string Url)
        {
			InitializeComponent();

			this.listener = listener;
            this.Url = Url;
        }

        private void OnClickRetry(object sender, EventArgs e)
        {
            listener.ReloadAfterNoInernet(Url);

        }

        private void OnClickExitApp(object sender, EventArgs e)
        {

        }
    }
}