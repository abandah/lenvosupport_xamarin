﻿using Acr.UserDialogs;
using MobApp_LSC.Files.constant;
using MobApp_LSC.Files.Constant;
using MobApp_LSC.Files.HybridWebViews;
using MobApp_LSC.Files.Models;
using MobApp_LSC.Files.Refit;
using MobApp_LSC.Files.Util;
using MobApp_LSC.Files.View;
using NavigationDrawer.MenuItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static NavigationDrawer.MenuItems.MasterPageItem;

namespace MobApp_LSC.Files.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : MasterDetailPage, OnResponse<MasterPageItem>
	{

		private string data;

		public MainPage(string data)
		{

			InitializeComponent();
			
			this.data = data;
			NavigationPage.SetHasNavigationBar(this, false);

			if (data != null || data != string.Empty)
			{
				Detail = new NavigationPage(new Profile(data))
				{
					BarBackgroundColor = Color.FromHex("#666666"),
					BarTextColor = Color.FromHex("#FFFFFF"),
					Icon = ""
				};
			}
			else
			{
				Detail = new NavigationPage(new Profile(Global.getLink()))
				{
					BarBackgroundColor = Color.FromHex("#666666"),
					BarTextColor = Color.FromHex("#FFFFFF"),
					Icon = ""
				};
			}

			CallApi_GetMenuItems();
			
		}

		public void fillMenu(List<MasterPageItem> menuList)
		{
			//if (Global.getGCM().Equals("GCM not set"))
			//{

			//}
			//else
			//{
			//	UpdateGCM.sendGCMtoserver(Global.getGCM());
			//}
			//menuList.Add(new MasterPageItem() { Title = "Settings", Link = "Settings", IsClickable = true });
			menuList.Add(new MasterPageItem() { Title =Helpers.TranslateExtension.Translate("Settings"), Link = "Settings", IsClickable = true });
			menuList.Add(new MasterPageItem() { Title =Helpers.TranslateExtension.Translate("Exit"), Link = "Exit", IsClickable = true });
			//menuList.Add(new MasterPageItem() { Title = "Exit", Link = "Exit", IsClickable = true });
			//navigationDrawerList.ItemsSource = menuList;

			StackLayout slmenu = new StackLayout();
			slmenu.HeightRequest = 28;
			slmenu.BackgroundColor = Color.FromHex("#FFFFFF");

			Grid menugrid = new Grid()
			{
				RowDefinitions =
					{
						new RowDefinition { Height = new GridLength(28, GridUnitType.Absolute) }
					},
				ColumnDefinitions =
					{
						new ColumnDefinition { Width = new GridLength(57, GridUnitType.Star) },
						new ColumnDefinition { Width = new GridLength(57, GridUnitType.Star) },
						new ColumnDefinition { Width = new GridLength(57, GridUnitType.Star) },
						//new ColumnDefinition { Width = new GridLength(57, GridUnitType.Star) }
					}
			};
			//----------------------------------------------------------------------------------------
			var tapGestureRecognizer = new TapGestureRecognizer();

			Image img1;
			menugrid.Children.Add(img1 = new Image { Source = "dasboard.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#5cb85c")}, 0, 0);
			img1.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(() =>
				{

					MasterPageItem it = new MasterPageItem();
					it.Link = Global.Link + "index.aspx";
					OnMenuItemSelected(it);


				})
			});

			Image img2;
			menugrid.Children.Add(img2 = new Image { Source = "user.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#4f99c6")}, 1, 0);
			img2.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(() =>
				{

					MasterPageItem it = new MasterPageItem();
					it.Link = Global.Link + "DetailedInfo.aspx";
					OnMenuItemSelected(it);


				})
			});

			Image img3;
			menugrid.Children.Add(img3 = new Image { Source = "tablet.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#e59729")}, 2, 0);
			img3.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(() =>
				{

				//	MasterPageItem it = new MasterPageItem();
					//it.Link = Global.Link + "QRBarcode.aspx";
				//	OnMenuItemSelected(it);


				})
			});

			//Image img4;
			//menugrid.Children.Add(img4 = new Image { Source = "sitemap.png", Margin = new Thickness(-3, 0, -3, -10), BackgroundColor = Color.FromHex("#b74635")}, 3, 0);
			//img4.GestureRecognizers.Add(new TapGestureRecognizer
			//{
			//	Command = new Command(() =>
			//	{

			//		//MasterPageItem it = new MasterPageItem();
			//		//it.Link = Global.Link + "OrganizaionalStructure/Organizaional_Structure.aspx";
			//		//OnMenuItemSelected(it);


			//	})
			//});
			//img4.IsVisible = false;
			//-----------------------------------------------------------------------------------------
			StackLayout sl = new StackLayout();
			ScrollView sv = new ScrollView();

			sl.Orientation = StackOrientation.Vertical;
			for (int i = 0; i < menuList.Count; i++)
			{
				MasterPageItem it = menuList[i];

				StackLayout con = new StackLayout();
				con.VerticalOptions = LayoutOptions.Fill;
				con.Orientation = StackOrientation.Horizontal;
				con.Padding = new Thickness(7, 7, 7, 5);
				con.Spacing = 6;

				Image img = new Image();
				//img.Source = it.Icon;
				img.VerticalOptions = LayoutOptions.Center;

				Label lab = new Label();
				lab.FontSize = 15;// Device.GetNamedSize(NamedSize.Small);
				lab.VerticalOptions = LayoutOptions.FillAndExpand;
				lab.FlowDirection= Device.FlowDirection;
				lab.HorizontalOptions = LayoutOptions.FillAndExpand;
				lab.TextColor = Color.Black;
				//lab.HorizontalTextAlignment = TextAlignment.Center;
				lab.Text = it.Title;

				Label lab2 = new Label();
				lab2.FontSize = 15;// Device.GetNamedSize(NamedSize.Small);
				lab2.VerticalOptions = LayoutOptions.FillAndExpand;
				lab2.HorizontalOptions = LayoutOptions.FillAndExpand;
				lab2.FlowDirection = Device.FlowDirection;
				lab2.TextColor = Color.FromHex("#666666");
				lab2.Text = it.Title;

				if (it.IsClickable)
				{
					con.GestureRecognizers.Add(
						new TapGestureRecognizer()
						{
							Command = new Command(() =>
							{
								var buttons = sl.Children;
								foreach (StackLayout bt in buttons)
								{

									bt.BackgroundColor = Color.White;
								}
								con.BackgroundColor = Color.FromHex("#666666");
								OnMenuItemSelected(it);

							})
						});
				}

				if (it.IsClickable)
				{
					con.Children.Add(img);
					con.Children.Add(lab);
				}
				else
				{
					con.Children.Add(lab2);
				}
				slmenu.Children.Add(menugrid);
				sl.Children.Add(slmenu);
				sl.Children.Add(con);
			}
			sv.Content = sl;

			myContentPage.Content = sv;
		}
		private void OnMenuItemSelected(MasterPageItem master)
		{
			var item = master;
			ContentPage page = item.GetTargetPage();
			//if(item.Link.Contains("UserNewRequest.aspx"))
			Detail = new NavigationPage(page)
			{
				BarBackgroundColor = Color.FromHex("#666666"),
				BarTextColor = Color.FromHex("#FFFFFF")
			};
			IsPresented = false;
		}
		protected override void OnAppearing()
		{
			base.OnAppearing();
			//CallApi_GetMenuItems();

		}


		private void CallApi_GetMenuItems()
		{

			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_USERID, Global.UserId },
				{ WSConstants.PARAM_LENVO_TOKEN, Global.getLenvoLoginToken() },};

			CallApi.HandelErrors(GetMenuItems, data, WSConstants.CALL_MENU, this);


		}
		private async Task<WSResponse<MasterPageItem>> GetMenuItems(Dictionary<string, string> data)
		{

			LenvoJobsAPIs<MasterPageItem> GetMenuItems = CallApi.Caller<LenvoJobsAPIs<MasterPageItem>>(Global.Link + WSConstants.API_URL);
			//   registerAPI.Registration(data).Wait();
			WSResponse<MasterPageItem> wSResponse = await GetMenuItems.GetMenuItems(data);
			//  Debug.WriteLine(wSResponse.getMessage());
			return wSResponse;
		}

		void OnResponse<MasterPageItem>.OnResult(int CallId, WSResponse<MasterPageItem> response)
		{

			switch (CallId)
			{
				case WSConstants.CALL_MENU:
					OnResult_CallApi_GetMenuItems(CallId, response);
					break;

				case WSConstants.NoInterNet:
					//shdaAsync();
					break;
			}

		}

		private void OnResult_CallApi_GetMenuItems(int callId, WSResponse<MasterPageItem> response)
		{
			if (response.Status == 1)
			{

				fillMenu(response.dataFromServer.DataRows);
				//SavePrefAndGlobizeAsync(response.LenvoLoginToken);
				//DisplayAlert("Success", "Success", "OK!");
			}
			else if (response.Status == 0)
			{
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), response.Message, Helpers.TranslateExtension.Translate("OK"));
			}
			else
			{
				dialogAPIAsync(response.Message);

			}


		}
		private async void dialogAPIAsync(string message)
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = message,
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("logout")
			});
			if (result)
			{
				CallApi_GetMenuItems();
			}
			else
			{
				Pref.CleanPref();
				App.Current.MainPage = new SplashPage(null);
			}
		}


	}
}
