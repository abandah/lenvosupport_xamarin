﻿using MobApp_LSC.Files.Interfaces;
using MobApp_LSC.Files.Models;
using MobApp_LSC.Files.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace MobApp_LSC.Files.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Settings : ContentPage
	{
        public Settings ()
		{
            LogoutData logoutdata = new LogoutData();
            logoutdata.pic = "logout.png";
            logoutdata.label = Helpers.TranslateExtension.Translate("Logout");
            BindingContext = logoutdata;

            InitializeComponent ();
            imgLogout.GestureRecognizers.Add(
             new TapGestureRecognizer()
             {
                 Command = new Command(() =>
                 {
                     Pref.CleanPref();
					// DependencyService.Get<IClearCookies>().Clear();
					 App.Current.MainPage = new SplashPage(null);
                 })
             });

            logout.GestureRecognizers.Add(
             new TapGestureRecognizer()
             {
                 Command = new Command(() =>
                 {
                     Pref.CleanPref();
					 DependencyService.Get<IClearCookies>().Clear();
					 App.Current.MainPage = new SplashPage(null);
                 })
             });
        }

        public static object NavigationItem { get; set; }
    }
}