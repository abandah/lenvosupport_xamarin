﻿using MobApp_LSC.Files.constant;
using MobApp_LSC.Files.Refit;
using NavigationDrawer.MenuItems;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobApp_LSC.Files
{
    class CallAPI<ArrayObjectType> :OnResponse<ArrayObjectType>
    {
		public event EventHandler <WSResponse<ArrayObjectType>> DataResponse;

		public CallAPI()
		{
			
		}
		public static CallAPI<ArrayObjectType> newInstance() {

			return new CallAPI<ArrayObjectType>();
		}

		public void CallApi_GetResponce(Func<Dictionary<string, string>, Task<WSResponse<ArrayObjectType>>> func, Dictionary<string, string> data)
		{

			CallApi.HandelErrors<ArrayObjectType>(func, data, WSConstants.CALL_MENU, this);

		}
	
		void OnResponse<ArrayObjectType>.OnResult(int CallId, WSResponse<ArrayObjectType> response)
		{
			DataResponse(this,response);
			
		}

		
	}
}
