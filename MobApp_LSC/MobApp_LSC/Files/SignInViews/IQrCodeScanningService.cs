﻿using System.Threading.Tasks;

namespace MobApp_LSC.Files.SignInViews
{
	public interface IQrCodeScanningService
	{
		Task<string> ScanAsync();
	}
}