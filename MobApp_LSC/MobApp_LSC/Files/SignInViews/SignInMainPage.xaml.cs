﻿using Acr.UserDialogs;
using MobApp_LSC.Files.constant;
using MobApp_LSC.Files.Refit;
using MobApp_LSC.Files.Util;
using MobApp_LSC.Files.Views;
using NavigationDrawer.MenuItems;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace MobApp_LSC.Files.SignInViews
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignInMainPage : ContentPage, INotifyPropertyChanged, OnResponse<MasterPageItem>
	{
		private string token = "";

		public SignInMainPage()
        {

			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);
		}

		public SignInMainPage(string link)
		{
			this.link = link;
			if (link.Equals("")) {
				this.link =Global.getLink();
			}
			InitializeComponent();
			
			btnLogin(null,null);

		}

		public ICommand ClickCommand => new Command<string>((url) =>
        {
            Device.OpenUri(new System.Uri(url));
        });

      
        

        //public List<CarouselData> MyDataSource { get; set; } // Must have default value or be set before the BindingContext is set.

        private int _position;
		private string link;

		public int Position { get { return _position; } set { _position = value; OnPropertyChanged(); } }

        private async void btnLogin(object sender, EventArgs e)
        {

			if (Global.getGCM().Equals("GCM not set"))
			{

			}
			else
			{
				UpdateGCM.sendGCMtoserver(Global.getGCM());
			}

			CheckInternetConnection(new Manual_Login());
			

		}
		private async void btnQRCode(object sender, EventArgs e)
        {

			//if (Global.getGCM().Equals("GCM not set"))
			//{

			//}
			//else
			//{
			//	UpdateGCM.sendGCMtoserver(Global.getGCM());

			//}

			try
			{
				var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
				if (status != PermissionStatus.Granted)
				{
					if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
					{
						await DisplayAlert(Helpers.TranslateExtension.Translate("Need_Camera"), Helpers.TranslateExtension.Translate("LenvoHRE_needs_camera_access"), Helpers.TranslateExtension.Translate("OK"));
					}

					var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
					//Best practice to always check that the key exists
					if (results.ContainsKey(Permission.Camera))
						status = results[Permission.Camera];
				}

				if (status == PermissionStatus.Granted)
				{
					var ScannerPage = new ZXingScannerPage() { Title = Helpers.TranslateExtension.Translate("Scan_QR_code") };

					ScannerPage.OnScanResult += (result) =>
					{
						ScannerPage.IsScanning = false;

						Device.BeginInvokeOnMainThread(() =>
						{
							Navigation.PopAsync();
							if (result != null)
							{
								String[] parameter = result.Text.Split(',');
								if (!parameter[0].Equals("LenvoHRE"))
									return;
								if (parameter.Length == 2)
									havingLink(parameter[1]);
								if (parameter.Length == 3)
									havingLink_Token(parameter[1], parameter[2]);

							}
						});
					};
					await Navigation.PushAsync(ScannerPage);
				}
				else if (status != PermissionStatus.Unknown)
				{
					await DisplayAlert(Helpers.TranslateExtension.Translate("Camera_Denied"), Helpers.TranslateExtension.Translate("Can_not_continue"), Helpers.TranslateExtension.Translate("OK"));
				}
			}
			catch (Exception ex)
			{

				ex.ToString();
			}
		}

		private async void havingLink(string link)
		{
			await Pref.SaveApplicationProperty(Pref.LINK, link.ToLower());
			Global.getLink();

			App.Current.MainPage = new NavigationPage(new SignInViews.SignInMainPage(link))
			{
				BarBackgroundColor = Color.FromHex("#666666"),
				BarTextColor = Color.FromHex("#FFFFFF")
			};
		}

		private void havingLink_Token(String link, String token)
		{
			Global.Link = link.ToLower();
			this.token = token;
			Dictionary<string, string> data = new Dictionary<string, string> {
					{WSConstants.PARAM_DEVIC_SERIAL,token},
					{WSConstants.PARAM_LENVO_TOKEN,token},
					 { WSConstants.PARAM_GCM_TOKEN,Global.getGCM()},
					 { WSConstants.PARAM_DEVICE_TYPE,constant.WSConstants.OS_TYPE}
				};
			Global.setDeviceSerial(token);
			CallApi_RegisterDevice(data);
		}

		void OnResponse<MasterPageItem>.OnResult(int CallId, WSResponse<MasterPageItem> response)
		{

			switch (CallId)
			{
				case WSConstants.REGISTER_DEVICE:
					OnResult_CallApi_RegisterDevice(CallId, response);
					break;

				case WSConstants.NoInterNet:
					shdaAsync();
					break;
			}

		}

		private async void shdaAsync()
		{
			var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
			{
				Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
				OkText = Helpers.TranslateExtension.Translate("Retry"),
				CancelText = Helpers.TranslateExtension.Translate("Close")
			});
			if (result && !this.token.Equals(""))
			{
				havingLink_Token(Global.Link, this.token);
			}
			else
			{
				App.Current.MainPage = new SignInMainPage();
			}
		}

		private void CallApi_RegisterDevice(Dictionary<string, string> data)
		{
			CallApi.HandelErrors<MasterPageItem>(RegisterDevice, data, WSConstants.REGISTER_DEVICE, this);

		}
		private async Task<WSResponse<MasterPageItem>> RegisterDevice(Dictionary<string, string> data)
		{

			LenvoJobsAPIs<MasterPageItem> registerAPI = CallApi.Caller<LenvoJobsAPIs<MasterPageItem>>(Global.Link + constant.WSConstants.API_URL);
			return await registerAPI.RegisterDevice(data);
		}
		private void OnResult_CallApi_RegisterDevice(int callId, WSResponse<MasterPageItem> response)
		{
			if (response.Status == 1)
			{
				SavePrefAndGlobizeAsync(response.UserId, response.LenvoLoginToken);
			}
			else
			{
				Util.ProgressDialog.HideProgressDialog();
				DisplayAlert(Helpers.TranslateExtension.Translate("Failed"), response.Message, Helpers.TranslateExtension.Translate("OK"));

			}
		}
		private async Task SavePrefAndGlobizeAsync(string userId, string lenvoLoginToken)
		{
			await Pref.SaveApplicationProperty(Pref.USER_ID, userId);
			await Pref.SaveApplicationProperty(Pref.LENVO_LOGIN_TOKEN, lenvoLoginToken);
			await Pref.SaveApplicationProperty(Pref.IS_REMEMBER_ME, true);
			await Pref.SaveApplicationProperty(Pref.LINK, Global.Link);
			Util.ProgressDialog.HideProgressDialog();

			App.Current.MainPage = new SplashPage(null);


		}

		private async void CheckInternetConnection(Page page)
		{
			if (Util.Validation.IsConnected())
			{
				//await App.Current.MainPage.Navigation.PushModalAsync(page);
				await App.Current.MainPage.Navigation.PushAsync(page);
			}
			else
			{
				var result = await UserDialogs.Instance.ConfirmAsync(new ConfirmConfig
				{
					Message = Helpers.TranslateExtension.Translate("There_is_no_internet_connection"),
					OkText = Helpers.TranslateExtension.Translate("Retry"),
					CancelText = Helpers.TranslateExtension.Translate("Close")
				});
				if (result)
				{
					CheckInternetConnection(page);
				}
				else
				{
					//App.Current.MainPage = new Exit();
				}
			}

		}

		

	}
}