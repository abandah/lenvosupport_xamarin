﻿using MobApp_LSC.Files;
using MobApp_LSC.Files.HybridWebViews;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files
{
	public class Global
	{
		public static bool Is_Remember_Me;
		public static string LenvoLoginToken;
		public static string UserId;
		public static string Link;
		public static string devic_serial="";
		public static string GCM;

		public static string getLenvoLoginToken()
		{
			//Debug.WriteLine("getLenvoLoginToken = " + LenvoLoginToken);
			return LenvoLoginToken;
		}
		public static void setLenvoLoginToken(string LoginToken)
		{
			//Debug.WriteLine("setLenvoLoginToken = " + LoginToken);
			LenvoLoginToken = LoginToken;
		}

		public static bool IsAllGloblized()
		{
			if (Global.Is_Remember_Me && !Global.LenvoLoginToken.Equals("") && !Global.UserId.Equals(""))
			{
				return true;
			}
			Is_Remember_Me = Pref.LoadApplicationProperty<bool>(Pref.IS_REMEMBER_ME);
			LenvoLoginToken = Pref.LoadApplicationProperty<string>(Pref.LENVO_LOGIN_TOKEN);
			//Link = Pref.LoadApplicationProperty<string>(Pref.LINK);
			UserId = Pref.LoadApplicationProperty<string>(Pref.USER_ID);
			if (Global.Is_Remember_Me && !Global.LenvoLoginToken.Equals("") && !Global.UserId.Equals(""))
			{
				return true;
			}
			return false;
		}
		public static string getLink() {
			//Link = Pref.LoadApplicationProperty<string>(Pref.LINK);
			return Link;
		}
		public async static void setGCM<T>(T GCM)
		{
			await Pref.SaveApplicationProperty<T>(Pref.GCM, GCM);

		}

		public static string getDeviceSerial(){
			devic_serial = Pref.LoadApplicationProperty<string>(Pref.DeviceSerial);
			return devic_serial;
		}

		public async static void setDeviceSerial<T>(T DeviceSerial){
			await Pref.SaveApplicationProperty<T>(Pref.DeviceSerial, DeviceSerial);
			getDeviceSerial();

		}

		public static String getGCM()
		{
			String GCM = Pref.LoadApplicationProperty<String>(Pref.GCM);
			GCM = GCM == null || GCM.Equals("") ? "GCM not set" : GCM;
			return GCM;
		}

		internal static void Clear()
		{
			Is_Remember_Me = false;
			LenvoLoginToken = string.Empty;
			UserId = string.Empty;
			//Link = string.Empty;
		}

	}
}
