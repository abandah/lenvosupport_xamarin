﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.Models
{
   public  class ATM
    {
		public int ID { get; set; }
		public string MachineName { get; set; }
		public int MachineId { get; set; }
		public double GPSLatitude { get; set; }
		public double GPSLongitude { get; set; }
		public int GPSRadius { get; set; }
		public double MachineDistance { get; set; }
	}
}
