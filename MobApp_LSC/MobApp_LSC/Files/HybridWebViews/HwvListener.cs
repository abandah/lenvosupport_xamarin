﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.HybridWebViews
{
    public interface HwvListener
    {
        void onLoaded();
        void OnProgressChanged(int newProgress);
        void ShouldOverrideUrlLoading(string url);
        void OnPageStarted(string url);
        void OnPageFinished(string url);
        void OnReceivedHttpError(string internalError);
        void OnReceivedError(string ErrorType);
        void NeedRetry(string url);

		void IOS_DidStartProvisionalNavigation();
		void IOS_DidFinishNavigation(string strUrl);
		void IOS_DecidePolicy(int  statusCode);
		void IOs_Retry();
	}
}
