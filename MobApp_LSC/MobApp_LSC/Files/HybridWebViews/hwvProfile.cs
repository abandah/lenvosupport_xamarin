﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MobApp_LSC.Files.HybridWebViews
{
    public class hwvProfile : WebView
    {

        Action<string> action;
        public HwvListener hwvListener;
        public  ContentPage contentPage;
        public EventHandler<EventArgs> GoBackOnNativeEventListener;

        public static readonly BindableProperty UriProperty = BindableProperty.Create(
            propertyName: "Uri",
            returnType: typeof(string),
            declaringType: typeof(hwvProfile),
            defaultValue: default(string));

        internal void setlistener(ContentPage contentPage)
        {
            this.contentPage = contentPage;
            this.hwvListener = contentPage as HwvListener;
        }

        public string Uri
        {
            get { return (string)GetValue(UriProperty); }
            set { SetValue(UriProperty, value); }
        }
	
		public void RegisterAction(Action<string> callback)
        {
            action = callback;
        }

        public void Cleanup()
        {
            action = null;
        }


        public void InvokeAction(string data)
        {
            if (action == null || data == null)
            {
                return;
            }
            action.Invoke(data);
        }
        public bool GoBack()
        {
            GoBackOnNative();
            return true;
        }

        private void GoBackOnNative()
        {
            GoBackOnNativeEventListener(this, EventArgs.Empty);
        }

    }
}
