﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.Refit
{
    interface OnResponse<T>
    {
       void OnResult(int CallId, WSResponse<T> response);
        
    }
}
