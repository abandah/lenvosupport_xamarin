﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobApp_LSC.Files.Refit
{
    public class WSResponse<T>
    {
        // public String Status;
        // private string Message;
        public String UserId;
        public int Status;
        public String LenvoLoginToken;
        public string InternalError;
        public bool HasError = false;
		public DataFromServer<T> dataFromServer;

		public DataFromServer<T> GetDataFromServer()
		{
			return dataFromServer;
		}

		public void SetDataFromServer(DataFromServer<T> value)
		{
			dataFromServer = value;
		}

		public string Message;

        public string getMessage()
        {
            if (HasError) return InternalError;
            else return Message;

        }

		public class DataFromServer<T>
		{
			public List<T> DataRows { get; set; }
		}
	}
}
