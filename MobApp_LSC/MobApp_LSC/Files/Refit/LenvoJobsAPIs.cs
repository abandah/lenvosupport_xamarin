﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MobApp_LSC.Files.Models;
using Refit;

namespace MobApp_LSC.Files.Refit
{
    interface LenvoJobsAPIs<T>
    {
      

        [Post("/LoginDeviceUser")]
        Task<WSResponse<T>> LoginDeviceUser([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/PunchByGPSLocation")]
        Task<WSResponse<T>> PunchByGPSLocation([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/Registration")]
        Task<WSResponse<T>> Registration([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/RegistrationByLinkedIn")]
        Task<WSResponse<T>> RegistrationByLinkedIn([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/RegistrationByFacebook")]
        Task<WSResponse<T>> RegistrationByFacebook([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

        [Post("/RegisterDevice")]
        Task<WSResponse<T>> RegisterDevice([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/accessToken")]
        Task<linkedinJSON> accessToken([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

        [Post("/token")]
        Task<googleJSON> accessTokengoogle([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/GetUserAllowedDevices")]
        Task<WSResponse<T>> GetUserAllowedDevices([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

        [Post("/LoginDevice")]
        Task<WSResponse<T>> LoginDevice([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

        [Post("/CheckDuplicateEmail")]
        Task<WSResponse<T>> CheckDuplicateEmail([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

        [Post("/ForgotPassword")]
        Task<WSResponse<T>> ForgotPassword([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);


		[Post("/GetClosestGPSATMs")]
        Task<WSResponse<T>> GetClosestGPSATMs([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

        [Post("/RegistrationByGmail")]
        Task<WSResponse<T>> RegistrationByGmail([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/GetMenuItems")]
		Task<WSResponse<T>> GetMenuItems([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/GetEntryTypes")]
		Task<WSResponse<T>> GetEntryTypes([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data);

		[Post("/TestAPI")]
        Task<WSResponse<T>> TestAPI();

        [Post("/TestAPI")]
        Task<WSResponse<T>> GetUser();
    }
}
