﻿using MobApp_LSC.Files.Models;
using Refit;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MobApp_LSC.Files.Refit
{
	class CallApi
	{
		public static string MainLink = "";

		public static T Caller<T>()
		{
			T t = RestService.For<T>(MainLink);
			//         T t = RestService.For<T>(new HttpClient
			//{
			//	BaseAddress = new Uri(MainLink),
			//	//Timeout = TimeSpan.FromSeconds(10)
			//});
			return t;
		}

		public static T Caller<T>(string Link)
		{
			T t = RestService.For<T>(Link);
			return t;
		}
		public static void HandelErrors<T>(Func<Dictionary<string, string>, Task<WSResponse<T>>> func, Dictionary<string, string> data, int CallId, OnResponse<T> onResponse)
		{
			HandelErrors(func, data, CallId, onResponse, true);
		}
		public async static void HandelErrors<T>(Func<Dictionary<string, string>, Task<WSResponse<T>>> func, Dictionary<string, string> data, int CallId, OnResponse<T> onResponse, bool ShowLoading)
		{
			if (ShowLoading)
				Util.ProgressDialog.ShowProgressDialog();

			WSResponse<T> response = new WSResponse<T>();
			if (Util.Validation.IsConnected())
			{
				try
				{

					response = await func(data);
				}
				catch (Exception exception)
				{
					response.InternalError = exception.Message;
					response.HasError = true;
					response.Status = -1;


				}
				finally
				{
					if (ShowLoading)
						Util.ProgressDialog.HideProgressDialog();
					onResponse.OnResult(CallId, response);


				}
			}
			else
			{
				response.InternalError = "No Internet Connection";
				response.HasError = true;
				response.Status = constant.WSConstants.NoInterNet;
				onResponse.OnResult(constant.WSConstants.NoInterNet, response);
			}
			// return response;
		}






	}


}
