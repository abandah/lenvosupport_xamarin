﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobApp_LSC.Files.Refit
{
	class RefitUtli
	{
		public static T getValue<T>(JObject json, string jsonPropertyName)
		{
			return json.SelectToken(jsonPropertyName).ToObject<T>();
		}

		public static JObject getValueAsJsonObject(JObject json, string jsonPropertyName)
		{
			return getValue<JObject>(json, jsonPropertyName);
		}

		public static JArray getValueAsJsonArray(JObject json, string jsonPropertyName)
		{
			return getValue<JArray>(json, jsonPropertyName);
		}

		public static List<JObject> getValueAsJsonList(JObject json, string jsonPropertyName)
		{
			return getValue<List<JObject>>(json, jsonPropertyName);
		}

		public static List<T> getValueAsList<T>(JObject json, string jsonPropertyName)
		{
			return getValue<List<T>>(json, jsonPropertyName);
		}

		internal static T getValueOnce<T>(JObject json, string objectName, string key)
		{
			List<JObject> table = getValueAsJsonList(json, objectName);
			return getValue<T>(table[0], key);
		}
	}
}
