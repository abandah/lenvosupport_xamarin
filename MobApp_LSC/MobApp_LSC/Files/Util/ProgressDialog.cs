﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MobApp_LSC.Files.Util
{
    public class ProgressDialog
    {

        private static MaskType mask = MaskType.Black;
        private static IUserDialogs dialogInstance = UserDialogs.Instance;
        private static IProgressDialog progressDialog;
        private static CancellationTokenSource cancelSrc = new CancellationTokenSource();

        private static ProgressDialogConfig config = new ProgressDialogConfig()
            .SetIsDeterministic(false)
            .SetMaskType(mask);
        public static void ShowProgressDialog(int progress)
        {
            //var i = progressDialog.GetType();

            if (progress < 100)
            {
				//if (progressDialog == null)
				//{
				//    progressDialog = dialogInstance.Progress();
				//}
				//progressDialog.Show();
				//progressDialog.PercentComplete = progress;
				//}
				ShowProgressDialog();
            }
            else
            {
				//if (progressDialog != null)
				//{
				//    progressDialog.Dispose();
				//    progressDialog = null;
				//}

				HideProgressDialog();

            }



        }


        public static void ShowProgressDialog()
        {
           // var i = progressDialog.GetType();

            if (progressDialog == null)
            {
                
                    progressDialog = dialogInstance.Progress(config);
                    progressDialog.Show();

              
            }

            if (progressDialog.IsShowing)
            {
                return;
            }

            progressDialog.Show();
           
        }

        public static void HideProgressDialog()
        {
           
                if (progressDialog != null)
            {
                if (progressDialog.IsShowing)
                {
                    progressDialog.Dispose();
                }
                progressDialog = null;

            }

         
        }
    }
}

