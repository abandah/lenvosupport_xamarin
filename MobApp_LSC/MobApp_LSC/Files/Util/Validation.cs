﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MobApp_LSC.Files.Util
{
    public class Validation
    {
        public static void CheckConnectionWithDialog(ContentPage page)
        {
			if (!CrossConnectivity.Current.IsConnected)
				Console.Write("No internet connection");
                 //page.Navigation.PushAsync(new Files.View.RegisterScreen());
            else
                return;
        }
        public static bool IsConnected()
        {
            return CrossConnectivity.Current.IsConnected;
               
        }
    }
}
