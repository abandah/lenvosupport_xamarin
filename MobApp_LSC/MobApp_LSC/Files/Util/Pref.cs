﻿using MobApp_LSC.Files;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MobApp_LSC.Files
{
    public class Pref
    {
        public static string IS_REMEMBER_ME = "isRememberMe";
        public static string USER_ID = "UserId";
        public static string LINK = "Link";
        public static string LENVO_TOKEN = "LenvoToken";
        public static string LENVO_LOGIN_TOKEN = "LenvoLoginToken";
		public static string GCM = "GCM";
		public static string DeviceSerial= "DeviceSerial";

		public static async Task SaveApplicationProperty<T>(string key, T value)
        {
            Xamarin.Forms.Application.Current.Properties[key] = value;
            await Xamarin.Forms.Application.Current.SavePropertiesAsync();
        }

        public static T LoadApplicationProperty<T>(string key)
        {
            T value;
            try
            {
                value = (T)Xamarin.Forms.Application.Current.Properties[key];
                return value;
            }
            catch
            {
                Type TType = typeof(T);
                if (TType == typeof(int))
                {
                    value = (T)Convert.ChangeType(-1, TType);
                }
                else if  (TType == typeof(bool)){
                    value = (T)Convert.ChangeType(false, TType);
                }
                else //if (TType == typeof(string))
                {
                    value = (T)Convert.ChangeType(string.Empty, TType);
                }
                return value;
            }

        }

        public static void CleanPref() {
           SaveApplicationProperty(Pref.IS_REMEMBER_ME, false);
            Global.Clear();
        }

		public static void SaveGCM(string deviceToken)
		{
			SavePrefAndGlobizeAsync(deviceToken);
		}
		private static async Task SavePrefAndGlobizeAsync(string GCM)
		{
			await Pref.SaveApplicationProperty(Pref.GCM, GCM);



		}

		// To save your property
		// await SaveApplicationProperty("isLoggedIn", true);

		// To load your property
		// bool isLoggedIn = LoadApplicationProperty<bool>("isLoggedIn");
	}
}
