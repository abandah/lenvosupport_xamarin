﻿using MobApp_LSC.Files.View;
using MobApp_LSC.Files.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace NavigationDrawer.MenuItems
{
	public class MasterPageItem
	{
		public string DashBoardUrl { get; set; }
		public string Title { get; set; }
		public string Icon { get; set; }
		public Boolean IsClickable { get; set; }
		public Boolean IsButton { get { return IsClickable; } }
		public Boolean InvertIsButton { get { return !IsClickable; } }
		public string Serial { get; set; }
		public string FK_ParentItemId { get; set; }
		public string Link { get; set; }

		public ContentPage GetTargetPage() {
			if (Link.Equals("Settings"))
			{
				return new Settings();
			}
			if (Link.Equals("Exit"))
			{
				return new Exit();
			}
			return new Profile(Link);
		}

		public class DataFromServer
		{
			public List<MasterPageItem> DataRows { get; set; }
		}
		public class RootObject
		{
			public DataFromServer DataFromServer { get; set; }
			public int Status { get; set; }
			public string Message { get; set; }
		}

	}
}
