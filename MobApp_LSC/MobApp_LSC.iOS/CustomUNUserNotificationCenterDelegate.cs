﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using UserNotifications;

namespace MobApp_LSC.iOS
{
	public class CustomUNUserNotificationCenterDelegate : UNUserNotificationCenterDelegate
	{
		string studentId = string.Empty, OrgNameSubTtl = string.Empty, moduleNmae = string.Empty, bodyText = string.Empty;

		public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
		{
			// Here you handle the user taps
			completionHandler();

			var remotePushData = response.Notification.Request.Content.UserInfo.ToDictionary(i => i.Key.ToString(), i => i.Value.ToString());

		}
		public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
		{
			completionHandler(UNNotificationPresentationOptions.Alert | UNNotificationPresentationOptions.Sound | UNNotificationPresentationOptions.Badge);

		}
	}
}