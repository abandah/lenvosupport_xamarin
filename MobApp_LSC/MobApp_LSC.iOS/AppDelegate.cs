﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Foundation;
using WindowsAzure.Messaging;
using Microsoft.AppCenter.iOS.Bindings;
using Microsoft.AppCenter.Push;
using SuaveControls.FloatingActionButton.iOS.Renderers;
using MobApp_LSC.Files;
using UIKit;

using Xamarin.Forms.GoogleMaps.iOS;
using UserNotifications;
using MobApp_LSC.Files.AzureInstance;
using Firebase.CloudMessaging;
using Firebase.InstanceID;

namespace MobApp_LSC.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, IMessagingDelegate
	{

		private SBNotificationHub Hub { get; set; }

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
			{

				// For iOS 10 display notification (sent via APNS)
				//UNUserNotificationCenter.Current.Delegate = this;

				var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
				UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
				{
					Console.WriteLine(granted);
				});
			}
			else
			{
				// iOS 9 or before
				var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
				var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
				UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			}

			UIApplication.SharedApplication.RegisterForRemoteNotifications();

			global::Xamarin.Forms.Forms.Init();
			ZXing.Net.Mobile.Forms.iOS.Platform.Init();
			Firebase.Core.App.Configure();
			MSAppCenter.SetLogLevel(MSLogLevel.Verbose);
			var installId = MSAppCenter.InstallId();
			Messaging.SharedInstance.Delegate = this;
			Messaging.SharedInstance.ShouldEstablishDirectChannel = true;
			FloatingActionButtonRenderer.InitRenderer();

			var platformConfig = new PlatformConfig
			{
				//ImageFactory = new CachingImageFactory()
			};

			// Register your app for remote notifications.

			//Xamarin.FormsGoogleMaps.Init("AIzaSyBosf4nAAOYelFDoA-JrUFzEvXuDajo2KI", platformConfig);
			Xamarin.FormsGoogleMaps.Init("AIzaSyB7pBXfkpxhh9MOoDtxbFz629HgRqegZpE", platformConfig);
			LoadApplication(new App(null));

			return base.FinishedLaunching(app, options);
		}

		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
			Messaging.SharedInstance.ApnsToken = deviceToken;
			string token = InstanceId.SharedInstance.Token;
			string fcm = Messaging.SharedInstance.FcmToken;
		}
		[Export("messaging:didReceiveRegistrationToken:")]
		public void DidReceiveRegistrationToken(Messaging messaging, string fcmToken)
		{
			Files.Util.UpdateGCM.UpdateGcm(fcmToken);

			// TODO: If necessary send token to application server.
			// Note: This callback is fired at each app startup and whenever a new token is generated.
		}
		[Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]
		public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification,
			Action<UNNotificationPresentationOptions> completionHandler)
		{
			Console.WriteLine("Handling iOS 11 foreground notification");
			completionHandler(UNNotificationPresentationOptions.Sound | UNNotificationPresentationOptions.Alert);
		}
		
		public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
		{
			// If you are receiving a notification message while your app is in the background,
			// this callback will not be fired till the user taps on the notification launching the application.
			// TODO: Handle data of notification

			// With swizzling disabled you must let Messaging know about the message, for Analytics
			//Messaging.SharedInstance.AppDidReceiveMessage (userInfo);

			// Print full message.
			Console.WriteLine(userInfo);
		}
		public void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
		{

			var userInfo = response.Notification.Request.Content.UserInfo;

			if (userInfo != null)
			{
				if (userInfo != null)
				{
					if (null != userInfo && userInfo.ContainsKey(new NSString("aps")))
					{
						NSDictionary aps = userInfo.ObjectForKey(new NSString("aps")) as NSDictionary;
						if (aps.ContainsKey(new NSString("category")))
						{
							string Link = (aps[new NSString("category")] as NSString).ToString();
							LoadApplication(new App(Link));
						}
					}
				}

			}

			completionHandler();
		}
		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			// If you are receiving a notification message while your app is in the background,
			// this callback will not be fired till the user taps on the notification launching the application.
			// TODO: Handle data of notification

			// With swizzling disabled you must let Messaging know about the message, for Analytics
			//Messaging.SharedInstance.AppDidReceiveMessage (userInfo);
			
			// Print full message.
			Console.WriteLine(userInfo);
			
			completionHandler(UIBackgroundFetchResult.NewData);
		}
	}
}
