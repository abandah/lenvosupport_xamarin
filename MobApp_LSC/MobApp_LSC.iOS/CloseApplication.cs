﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Foundation;
using UIKit;

namespace MobApp_LSC.iOS
{
	public class CloseApplication : Files.Interfaces.ICloseApplication
	{
		public void closeApplication()
		{
			Thread.CurrentThread.Abort();
		}
	}
}