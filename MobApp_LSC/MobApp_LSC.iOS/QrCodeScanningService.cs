﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using MobApp_LSC.Files.SignInViews;
using QRCodeScanner.Services;
using UIKit;
using ZXing.Mobile;

namespace MobApp_LSC.iOS
{
	public class QrCodeScanningService : IQrCodeScanningService
	{

		public async Task<String> ScanAsync()
		{
				var optionsCustom = new MobileBarcodeScanningOptions()
				{
					TryHarder = true,
					UseFrontCameraIfAvailable = true
				};
				var scanner = new MobileBarcodeScanner()
				{
					TopText = "Aproxime a câmera do QR Code",
					BottomText = "Toque na tela para focar"
				};

				var scanResults = await scanner.Scan(optionsCustom);

			return scanResults.Text;
		}
	}
}