﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using MobApp_LSC.Files.HybridWebViews;
using MobApp_LSC.iOS;
using UIKit;
using WebKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(hwvProfile), typeof(UIViewProfile))]
namespace MobApp_LSC.iOS
{
	public class UIViewProfile : ViewRenderer<hwvProfile, WKWebView>, IWKScriptMessageHandler, IWKNavigationDelegate
	{
		HwvListener listener;

		const string JavaScriptFunction = "function invokeCSharpAction(data){window.webkit.messageHandlers.invokeAction.postMessage(data);}";
		WKUserContentController userController;
		string uri;

		protected override void OnElementChanged(ElementChangedEventArgs<hwvProfile> e)
		{
			base.OnElementChanged(e);
			try
			{
				uri = e.NewElement.Uri;
			}
			catch (Exception ex)
			{
				listener.IOs_Retry();
				return;
			}
			listener = e.NewElement.hwvListener;
			ContentPage contentPage = e.NewElement.contentPage;

			if (Control == null)
			{
				userController = new WKUserContentController();
				var script = new WKUserScript(new NSString(JavaScriptFunction), WKUserScriptInjectionTime.AtDocumentEnd, false);
				userController.AddUserScript(script);
				userController.AddScriptMessageHandler(this, "invokeAction");

				var config = new WKWebViewConfiguration { UserContentController = userController };
				var webView = new WKWebView(Frame, config);
				webView.NavigationDelegate = (WebKit.IWKNavigationDelegate)this;

				SetNativeControl(webView);
			}
			//if (e.OldElement != null)
			//{
			//    userController.RemoveAllUserScripts();
			//    userController.RemoveScriptMessageHandler("invokeAction");
			//    var hybridWebView = e.OldElement as hwvProfile;
			//    hybridWebView.Cleanup();
			//}
			if (e.NewElement != null)
			{
				e.NewElement.GoBackOnNativeEventListener += (IntentSender, args) =>
				{
					Control.GoBack();
				};
				//Control.LoadRequest(new NSUrlRequest(new NSUrl(Element.Uri)));
			}

			Control.LoadRequest(new NSUrlRequest(new NSUrl(uri)));

		}
		public void DidReceiveScriptMessage(WKUserContentController userContentController, WKScriptMessage message)
		{
			Element.InvokeAction(message.Body.ToString());
		}
		[Export("webView:didFailNavigation:withError:")]
		public void DidFailNavigation(WKWebView webView, WKNavigation navigation, NSError error)
		{
			// If navigation fails, this gets called
			Console.WriteLine("DidFailNavigation");
		}

		[Export("webView:didFailProvisionalNavigation:withError:")]
		public void DidFailProvisionalNavigation(WKWebView webView, WKNavigation navigation, NSError error)
		{
			// If navigation fails, this gets called
			Console.WriteLine("DidFailProvisionalNavigation");
		}
		[Export("webView:didStartProvisionalNavigation:")]
		public void DidStartProvisionalNavigation(WKWebView webView, WKNavigation navigation)
		{
			listener.IOS_DidStartProvisionalNavigation();

		}
		[Export("webView:didFinishNavigation:")]
		public void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
		{
			listener.IOS_DidFinishNavigation(webView.Url.ToString().ToLower());

		}
		[Foundation.Export("webView:decidePolicyForNavigationResponse:decisionHandler:")]
		public virtual void DecidePolicy(WKWebView webView, WKNavigationResponse navigationResponse, Action<WKNavigationResponsePolicy> decisionHandler)
		{
			NSHttpUrlResponse response = (NSHttpUrlResponse)navigationResponse.Response;

			//listener.IOS_DecidePolicy(response.StatusCode+"");
			listener.IOS_DecidePolicy(Convert.ToInt32(response.StatusCode));
			if (response.StatusCode == 404)
			{
				//handel error
			}

			decisionHandler(WKNavigationResponsePolicy.Allow);
		}

	}
}