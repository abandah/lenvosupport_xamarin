﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using MobApp_LSC.Files.HybridWebViews;
using Xamarin.Forms;

namespace MobApp_LSC.Droid.Clients
{
    class MyWebViewClient : WebViewClient
    {
        private string uri;
        private HwvListener listener;
        private ContentPage contentPage;

        public MyWebViewClient(string uri, HwvListener listener, ContentPage contentPage)
        {
            this.uri = uri;
            this.listener = listener;
            this.contentPage = contentPage;
        }

        public override bool ShouldOverrideUrlLoading(Android.Webkit.WebView view, string url)
        {
            try
            {
                view.LoadUrl(url);
            }
            catch(Exception e) {
                listener.NeedRetry(url);
            }

            listener.ShouldOverrideUrlLoading(url);
            return true;
        }
        public override void OnPageStarted(Android.Webkit.WebView view, string url, Bitmap favicon)
        {
            listener.OnPageStarted(url.ToLower());
			if (Files.Util.Validation.IsConnected())
			{
				base.OnPageStarted(view, url.ToLower(), favicon);
			}
			else {
				listener.NeedRetry(url);
			}
				
        }
        public override void OnPageFinished(Android.Webkit.WebView view, string url)
        {
            listener.OnPageFinished( url);
            base.OnPageFinished(view, url);
        }
        public override void OnReceivedHttpError(Android.Webkit.WebView view, IWebResourceRequest request, WebResourceResponse errorResponse)
        {
            string Response = errorResponse.ReasonPhrase.ToLower();
            string internalError = "Internal Server Error".ToLower(); ;
            if (Response.Equals(internalError)) { 
            listener.OnReceivedHttpError(internalError);
        }
            base.OnReceivedHttpError(view, request, errorResponse);
        }
        public override void OnReceivedError(Android.Webkit.WebView view, IWebResourceRequest request, WebResourceError error)
        {
            if (error.ErrorCode == ClientError.HostLookup)
            {
                listener.OnReceivedError("-2");
                return;
            }
            listener.OnReceivedError(error.ErrorCode.ToString());
            

            base.OnReceivedError(view, request, error);
        }



    }
}