﻿using Android.App;
using Android.Content;
using Android.Webkit;
using MobApp_LSC.Files.HybridWebViews;
using Xamarin.Forms;

namespace MobApp_LSC.Droid.Clients
{
    class MyWebChromeViewClient : WebChromeClient
	{
		MainActivity activity;
		private string uri;
		private HwvListener listener;
		private ContentPage contentPage;
		//private UploadService uloadService;
		private static int filechooser = 1;
		private IValueCallback message;

		public MyWebChromeViewClient(MainActivity s)
		{
			this.activity = s;
		}

		public void setMyWebChromeViewClient(Context s)
		{
			this.activity = (MainActivity)s;
			//	uloadService = new UploadService();

		}

		public MyWebChromeViewClient(string uri, HwvListener listener, ContentPage contentPage)
		{

			this.uri = uri;
			this.listener = listener;
			this.contentPage = contentPage;
		}
		public override bool OnShowFileChooser(Android.Webkit.WebView webView, IValueCallback filePathCallback, FileChooserParams fileChooserParams)
		{
			this.message = filePathCallback;
			Intent chooserIntent = fileChooserParams.CreateIntent();
			chooserIntent.AddCategory(Intent.CategoryOpenable);
			this.activity.StartActivity(Intent.CreateChooser(chooserIntent, "File Chooser"), filechooser, this.OnActivityResult);
			return true;
		}
		public override void OnProgressChanged(Android.Webkit.WebView view, int newProgress)
		{
			base.OnProgressChanged(view, newProgress);
			//  Toast.MakeText(context,""+newProgress,0).Show();
			listener.OnProgressChanged(newProgress);


			//  Files.Util.ProgressBar.ShowProgressBar(contentPage);
			//iles.Util.ProgressBar.ShowProgressBar(App.Current.MainPage);
		}

		private void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			if (data != null)
			{
				if (requestCode == filechooser)
				{
					if (null == this.message)
					{
						return;
					}

					this.message.OnReceiveValue(WebChromeClient.FileChooserParams.ParseResult((int)resultCode, data));
					this.message = null;
				}
			}
		}



	}
}