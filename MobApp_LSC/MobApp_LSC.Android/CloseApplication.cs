﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;

namespace MobApp_LSC.Droid
{
	public class CloseApplication :Files.Interfaces.ICloseApplication
	{

		public void closeApplication()
		{
			var activity = (Activity)Forms.Context;
			activity.FinishAffinity();
		}
	}
}