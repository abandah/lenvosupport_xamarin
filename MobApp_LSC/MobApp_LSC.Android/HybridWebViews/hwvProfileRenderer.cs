﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using MobApp_LSC.Droid.HybridWebViews;
using MobApp_LSC.Files.HybridWebViews;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(hwvProfile), typeof(hwvProfileRenderer))]
namespace MobApp_LSC.Droid.HybridWebViews
{
	public class hwvProfileRenderer : ViewRenderer<hwvProfile, Android.Webkit.WebView>
	{
		const string JavascriptFunction = "function invokeCSharpAction(data){jsBridge.invokeAction(data);}";
		Context _context;

		public hwvProfileRenderer(Context context) : base(context)
		{
			_context = context;
		}

		protected override void OnElementChanged(ElementChangedEventArgs<hwvProfile> e)
		{
			base.OnElementChanged(e);

			string uri = e.NewElement.Uri;
			HwvListener listener = e.NewElement.hwvListener;
			ContentPage contentPage = e.NewElement.contentPage;

			// hwvListener

			if (Control == null)
			{
				e.NewElement.GoBackOnNativeEventListener += (IntentSender, args) => { };
				var webView = new Android.Webkit.WebView(_context);
				webView.Settings.JavaScriptEnabled = true;
				webView.Settings.AllowFileAccess = true;
				webView.Settings.AllowFileAccessFromFileURLs = true;
				webView.Settings.AllowContentAccess = true;
				webView.Settings.AllowUniversalAccessFromFileURLs = true;
				
				// webView.SetWebViewClient(new JavascriptwvcProfile($"javascript: {JavascriptFunction}"));
				Clients.MyWebChromeViewClient my = new Clients.MyWebChromeViewClient(uri, listener, contentPage);
				my.setMyWebChromeViewClient(_context);

				webView.SetWebChromeClient(my);
				webView.SetWebViewClient(new Clients.MyWebViewClient(uri, listener, contentPage));
				SetNativeControl(webView);
				//webView.Settings.UserAgentString = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
				//webView.Settings.UserAgentString = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36";
			}
			if (e.OldElement != null)
			{
				Control.RemoveJavascriptInterface("jsBridge");
				var hybridWebView = e.OldElement as hwvProfile;
				hybridWebView.Cleanup();
			}
			if (e.NewElement != null)
			{
				e.NewElement.GoBackOnNativeEventListener += (IntentSender, args) =>
				{
					Control.GoBack();
				};
				var customWebView = Element as hwvProfile;


				Control.AddJavascriptInterface(new JSBridgeProfile(this), "jsBridge");
				Control.SetDownloadListener(new CustomDownloadListener());
				Control.LoadUrl(e.NewElement.Uri);
			}

		}
		public class CustomDownloadListener : Java.Lang.Object, IDownloadListener
		{
			public void OnDownloadStart(string url, string userAgent, string contentDisposition, string mimetype, long contentLength)
			{

				string cookie = CookieManager.Instance.GetCookie(url);
				var source = Android.Net.Uri.Parse(url);
				DownloadManager.Request request = new DownloadManager.Request(source);
				request.AddRequestHeader("Cookie", cookie);
				request.AllowScanningByMediaScanner();
				request.SetNotificationVisibility(DownloadVisibility.VisibleNotifyCompleted);
				request.SetDestinationInExternalFilesDir(Forms.Context, Android.OS.Environment.DirectoryDownloads, "PDFReport.pdf");
				DownloadManager dm = (DownloadManager)Android.App.Application.Context.GetSystemService(Context.DownloadService);
				dm.Enqueue(request);

			}

		}
	}
}