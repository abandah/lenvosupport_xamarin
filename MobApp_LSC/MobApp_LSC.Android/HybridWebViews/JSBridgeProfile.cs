﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Interop;

namespace MobApp_LSC.Droid.HybridWebViews
{
    public class JSBridgeProfile : Java.Lang.Object
    {
        readonly WeakReference<hwvProfileRenderer> hybridWebViewRenderer;

        public JSBridgeProfile(hwvProfileRenderer hybridRenderer)
        {
            hybridWebViewRenderer = new WeakReference<hwvProfileRenderer>(hybridRenderer);
        }

        [JavascriptInterface]
        [Export("invokeAction")]
        public void InvokeAction(string data)
        {
            hwvProfileRenderer hybridRenderer;

            if (hybridWebViewRenderer != null && hybridWebViewRenderer.TryGetTarget(out hybridRenderer))
            {
                hybridRenderer.Element.InvokeAction(data);
            }
        }
    }
}