﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs.Infrastructure;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Iid;
using WindowsAzure.Messaging;
using Microsoft.WindowsAzure.MobileServices;
using MobApp_LSC.Files;

namespace MobApp_LSC.Droid.Service
{
	[Service]
	[IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
	public class MyFirebaseIIDService : FirebaseInstanceIdService
	{
		const string TAG = "MyFirebaseIIDService";
		NotificationHub hub;

		public override void OnTokenRefresh()
		{
			var refreshedToken = FirebaseInstanceId.Instance.Token;
			Log.Debug(TAG, "FCM token: " + refreshedToken);
			SendRegistrationToServer(refreshedToken);
		}

		void SendRegistrationToServer2(string token)
		{
			Task.Run(async () =>
			{
				MobileServiceClient client = new MobileServiceClient("http://my-mobile-app-url.azurewebsites.net");
				await AzureNotificationHubService.RegisterAsync(client.GetPush(), token);
			});
		}

		void SendRegistrationToServer(string token)
		{
			// Register with Notification Hubs
			hub = new NotificationHub(Files.AzureInstance.Constants.NotificationHubName,
										Files.AzureInstance.Constants.ListenConnectionString, this);

			//var tags = new List<string>() { };
			//var regID = hub.Register(token, tags.ToArray()).RegistrationId;
			Files.Util.UpdateGCM.UpdateGcm(token);

			//Log.Debug(TAG, $"Successful registration of ID {regID}");
		}
	}
}