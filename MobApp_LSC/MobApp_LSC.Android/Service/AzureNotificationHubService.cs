﻿using System;
using System.Threading.Tasks;
using Android.Util;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json.Linq;

namespace MobApp_LSC.Droid.Service
{
	class AzureNotificationHubService
	{
		const string TAG = "AzureNotificationHubService";

		public static async Task RegisterAsync(Push push, string token)
		{
			try
			{
				const string templateBody = "{\"data\":{\"message\":\"$(messageParam)\"}}";
				JObject templates = new JObject();
				templates["genericMessage"] = new JObject
				{
					{"body", templateBody}
				};

				//await push.RegisterAsync(token, templates);
			}
			catch (Exception ex)
			{
				Log.Error(TAG, "Could not register with Notification Hub: " + ex.Message);
			}
		}
	}
}