﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Util;
using Firebase.Messaging;
using Android.Support.V4.App;
using Build = Android.OS.Build;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Iid;

namespace MobApp_LSC.Droid.Service
{
	[Service]
	[IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
	public class MyFirebaseMessagingService : FirebaseMessagingService
	{
		const string TAG = "MyFirebaseMsgService";
		public override void OnMessageReceived(RemoteMessage message)
		{
			Log.Debug(TAG, "From: " + message.From);
			if (message.GetNotification() != null)
			{
				//These is how most messages will be received
				Log.Debug(TAG, "Notification Message Body: " + message.GetNotification().Body);
				SendNotification(message.GetNotification());
			}
			else
			{
				//Only used for debugging payloads sent from the Azure portal
				//SendNotification(message.Data.Values.First());

			}
		}

		void SendNotification(RemoteMessage.Notification message)
		{
			var intent = new Intent(this, typeof(MainActivity));
			intent.AddFlags(ActivityFlags.ClearTop);
			intent.PutExtra("Link", message.ClickAction);
			var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);
			var notificationBuilder = new NotificationCompat.Builder(this)
						.SetContentTitle(message.Title)
						.SetSmallIcon(Resource.Drawable.lenvohre)
						.SetContentText(message.Body)
						.SetAutoCancel(true)
						.SetShowWhen(false)
						.SetContentIntent(pendingIntent);

			if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
			{
				notificationBuilder.SetChannelId(MainActivity.CHANNEL_ID);
			}

			var notificationManager = NotificationManager.FromContext(this);

			notificationManager.Notify(0, notificationBuilder.Build());
		}
	}
}