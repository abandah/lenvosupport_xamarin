﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Util;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Content;
using Android.Support.V4.Content;
using Microsoft.AppCenter.Push;
using Acr.UserDialogs;
using Xamd.ImageCarousel.Forms.Plugin.Droid;
using Xamarin.Forms.GoogleMaps.Android;
using Plugin.Permissions;
using Firebase.Iid;
using Plugin.CurrentActivity;

namespace MobApp_LSC.Droid
{

	[Activity(Label = "Lenvosoft Support", Icon = "@drawable/lenvohre", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		public const string TAG = "MainActivity";
		internal static readonly string CHANNEL_ID = "my_notification_channel";
		private Action<int, Result, Intent> resultCallbackvalue;

		public void StartActivity(Intent intent, int requestCode, Action<int, Result, Intent> resultCallback)
		{
			this.resultCallbackvalue = resultCallback;
			StartActivityForResult(intent, requestCode);
		}
		protected override void OnCreate(Bundle savedInstanceState)
		{

			IsPlayServicesAvailable();
			CreateNotificationChannel();

			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;
			base.OnCreate(savedInstanceState);
			if (Convert.ToInt32(Build.VERSION.SdkInt) > 21)
			{
				Window.ClearFlags(WindowManagerFlags.TranslucentStatus);
				Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
				Window.SetNavigationBarColor(new Android.Graphics.Color(ContextCompat.GetColor(this, Resource.Color.background_floating_material_dark)));
			}
			global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
			UserDialogs.Init(this);
			ImageCarouselRenderer.Init();
			Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, savedInstanceState);

			var platformConfig = new PlatformConfig
			{
				//BitmapDescriptorFactory = new CachingNativeBitmapDescriptorFactory()
			};
			var fcmToken = FirebaseInstanceId.Instance.Token;
			//var refreshedGCMToken = FirebaseInstanceId.Instance.Token;
			//Files.Util.UpdateGCM.UpdateGcm(refreshedGCMToken);
			CrossCurrentActivity.Current.Init(Application);
			Xamarin.FormsGoogleMaps.Init(this, savedInstanceState); // initialize for Xamarin.Forms.GoogleMaps
			Push.SetSenderId("323124246080");
			ZXing.Net.Mobile.Forms.Android.Platform.Init();
			if (Intent.Extras != null)
			{
				string newString = Intent.Extras.GetString("Link");
				LoadApplication(new App(newString));
			}
			else
			{
				LoadApplication(new App(null));
			}
			//LoadApplication(new App());
			
			//Xamarin.Forms.Application.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
		}

		public bool IsPlayServicesAvailable()
		{
			return true;
		}
		private void CreateNotificationChannel()
		{
			if (Build.VERSION.SdkInt < BuildVersionCodes.O)
			{
				// Notification channels are new in API 26 (and not a part of the
				// support library). There is no need to create a notification
				// channel on older versions of Android.
				return;
			}

			var channelName = CHANNEL_ID;
			var channelDescription = string.Empty;
			var channel = new NotificationChannel(CHANNEL_ID, channelName, NotificationImportance.Default)
			{
				Description = channelDescription
			};

			var notificationManager = (NotificationManager)GetSystemService(NotificationService);
			notificationManager.CreateNotificationChannel(channel);
		}

		public event Action<int, Result, Intent> ActivityResult;

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
		{
			PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
			base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}
		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (this.resultCallbackvalue != null)
			{
				this.resultCallbackvalue(requestCode, resultCode, data);
				this.resultCallbackvalue = null;
			}
		}

	}
}