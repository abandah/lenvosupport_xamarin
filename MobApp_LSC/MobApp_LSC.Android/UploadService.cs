﻿using Android.App;
using Android.Content;
using Android.Webkit;
using MobApp_LSC.Droid;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static Android.Webkit.WebChromeClient;

namespace MobApp_LSC.Files.Interfaces
{
    class UploadService 
	{
		public Task<bool> RequestBluetooth(FileChooserParams fileChooserParams)
		{
			var activity = (MainActivity)Forms.Context;
			var listener = new ActivityResultListener(activity);

			const int RequestEnableBt = 2;
			Intent chooserIntent = fileChooserParams.CreateIntent();
			chooserIntent.AddCategory(Intent.CategoryOpenable);
			activity.StartActivityForResult(Intent.CreateChooser(chooserIntent, "File Chooser"),1);
			
			return listener.Task;
		}
		class ActivityResultListener
		{
			private TaskCompletionSource<bool> Complete = new TaskCompletionSource<bool>();
			public Task<bool> Task { get { return this.Complete.Task; } }

			public ActivityResultListener(MainActivity activity)
			{
				// subscribe to activity results
				activity.ActivityResult += OnActivityResult;
			}

			private void OnActivityResult(int requestCode, Result resultCode, Intent data)
			{
				// unsubscribe from activity results
				var context = Forms.Context;
				var activity = (MainActivity)context;
				activity.ActivityResult -= OnActivityResult;

				// process result
				if (requestCode == 1)
				{
					WebChromeClient.FileChooserParams.ParseResult((int)resultCode, data);
				}
				
			}
		}
	}
}
