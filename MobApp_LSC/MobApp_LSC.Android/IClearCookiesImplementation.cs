﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using MobApp_LSC.Droid;
using MobApp_LSC.Files.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(IClearCookiesImplementation))]
namespace MobApp_LSC.Droid
{
	public class IClearCookiesImplementation : IClearCookies
	{

		public void Clear()
		{
			var cookieManager = CookieManager.Instance;
			cookieManager.RemoveAllCookie();
		}
	}
}